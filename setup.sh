# Mac settings

## stop workspace autoswitching
defaults write com.apple.dock workspaces-auto-swoosh -bool NO

## scroll direction
defaults write -g com.apple.swipescrolldirection -boolean YES #scroll direction

## change screenshot path:
mkdir ~/Pictures/screenshots
defaults write com.apple.screencapture location ~/Pictures/screenshots/ && killall SystemUIServer



# install developer tools
xcode-select --install

#install homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# adding homebrew to path
(echo; echo 'eval "$(/opt/homebrew/bin/brew shellenv)"') >> /Users/martinsiklar/.zprofile
    eval "$(/opt/homebrew/bin/brew shellenv)"

#install google chrome
brew install google-chrome

#remove icons from dock
brew install dockutil

dockutil --remove 'TV'
dockutil --remove 'App Store'
dockutil --remove 'Apple Music'
dockutil --remove 'Music'
dockutil --remove 'FaceTime'
dockutil --remove 'Maps'    
dockutil --remove 'Freeform'
dockutil --remove 'Contacts'
dockutil --remove 'Photos'  
dockutil --remove 'Messages'
dockutil --remove 'Safari'  
dockutil --remove 'Mail'


# zsh 
brew install zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions

brew install --cask iterm2

brew install powerlevel10k
echo "source $(brew --prefix)/share/powerlevel10k/powerlevel10k.zsh-theme" >>~/.zshrc

## install fonts
p10k configure


# let's brew
## java
brew install java
sudo ln -sfn /opt/homebrew/opt/openjdk/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk

## python
brew install python3
curl -sSL https://install.python-poetry.org | python3 -
poetry config --local virtualenvs.in-project true

## R
brew install r

## devOps & cloud
brew install --cask docker
brew install terraform
brew install awscli

## dev IDEs
brew install --cask visual-studio-code
brew install --cask pycharm-ce
brew install --cask rstudio

## pw manager
brew install --cask keeweb

## security
brew install --cask nordvpn

## messaging 
brew install --cask whatsapp

## productivity
brew install --cask notion

## geospatial tools
#brew install gdal --HEAD
#brew install gdal
brew install --cask qgis

# Folders
mkdir Code 
