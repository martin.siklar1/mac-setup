# Mac Setup Script
to help with a speedy reinstallation of my core dev components after a reset. 


**Disclaimer:**
 Please make sure to look through the script before running it to understand what it does and changes on your system.
 It's best to run the script line by line to have better control over the installed components.

**Software and tools:**
- brew
- Python (poetry)
- Java
- docker
- terraform
- VScode
- Google Chrome
- awscli
- Pycharm-ce
- whatsapp messaging
- keeweb
- R
- RStudio

**Productivity:**
- Notion

**Geospatial Tools:**
- GDAL
- QGIS

**Mac Settings:**
- dock settings (remove unnecessary apps)
- scroll direction (natural)
- stop workspace switch

**Command Line:**
- zsh
- powerlevel10k


### for powerlevel10k
reference: https://github.com/romkatv/powerlevel10k

Configure your terminal to use font:

- iTerm2: Type p10k configure and answer Yes when asked whether to install Meslo Nerd Font. Alternatively, open iTerm2 → Preferences → Profiles → Text and set Font to MesloLGS NF.

- pple Terminal: Open Terminal → Preferences → Profiles → Text, click Change under Font and select MesloLGS NF family.

- Visual Studio Code: Open File → Preferences → Settings (PC) or Code → Preferences → Settings (Mac), enter terminal.integrated.fontFamily in the search box at the top of Settings tab and set the value below to MesloLGS NF. Consult this screenshot to see how it should look like or see this issue for extra information.

# Useful related links:

https://sourabhbajaj.com/mac-setup/SystemPreferences/